package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ocrsample1Application {

	public static void main(String[] args) {
		SpringApplication.run(Ocrsample1Application.class, args);
	}

}
