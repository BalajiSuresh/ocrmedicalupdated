package com.example.demo.service;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dao.OcrDao;
import com.google.gson.Gson;

import net.sourceforge.tess4j.Tesseract;

@Service
public class OcrServiceImpl implements OcrService {
	 @Autowired
	 OcrDao ocrdao;


	@Override
	public void putvalue(MultipartFile file) throws Exception {
		// TODO Auto-generated method stub
/*		String ext = FilenameUtils.getExtension(file.getOriginalFilename());
		if (!"png".equals(ext) && !"jpg".equals(ext)) {*/
		
		String resultado = "";
		Gson gson = new Gson();
		
		
		try {
			BufferedImage img = ImageIO.read(file.getInputStream());
	        Tesseract tesseract = new Tesseract();
	        tesseract.setDatapath("C:\\Users\\Admin\\Downloads\\Tess4J-1.3-src\\Tess4J\\tessdata\\");
	        resultado = "";	     
            tesseract.setLanguage("eng");
			resultado = tesseract.doOCR(img);
			ocrdao.putvalue(resultado);
			
		} catch (IOException e) {
			throw new Exception("Error");
		}
	 }
		
	

}
