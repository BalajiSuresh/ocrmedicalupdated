package com.example.demo.controller;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.service.OcrService;
import com.google.cloud.vision.v1.AnnotateImageRequest;
import com.google.cloud.vision.v1.AnnotateImageResponse;
import com.google.cloud.vision.v1.BatchAnnotateImagesResponse;
import com.google.cloud.vision.v1.EntityAnnotation;
import com.google.cloud.vision.v1.Feature;
import com.google.cloud.vision.v1.Feature.Type;
import com.google.cloud.vision.v1.Image;
import com.google.cloud.vision.v1.ImageAnnotatorClient;
import com.google.protobuf.ByteString;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;


@Controller
public class OcrController {
    public static final String uploadingDir = System.getProperty("user.dir") + "/uploadingDir/";
    @Autowired
    OcrService ocrservice;
    

    @RequestMapping("/")
    public String uploading(Model model) {
        File file = new File(uploadingDir);
        model.addAttribute("files", file.listFiles());
        return "uploading";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<String> OcrFetch(@RequestParam(name="file") MultipartFile file) throws Exception{
    	ocrservice.putvalue(file);
		/*return ResponseEntity.ok(gson.toJson(resultado));*/
		return null;						
	}
    @RequestMapping(value = "/test", method = RequestMethod.POST)
    public void testMethod(String filePath) throws Exception,IOException {
    	      //ConvertPDFPagesToImages();
    	      filePath = "C:\\Users\\Admin\\Downloads\\0001.jpg";
    		  List<AnnotateImageRequest> requests = new ArrayList<>();

    		  ByteString imgBytes = ByteString.readFrom(new FileInputStream(filePath));

    		  Image img = Image.newBuilder().setContent(imgBytes).build();
    		  Feature feat = Feature.newBuilder().setType(Type.TEXT_DETECTION).build();
    		  AnnotateImageRequest request =
    		      AnnotateImageRequest.newBuilder().addFeatures(feat).setImage(img).build();
    		  requests.add(request);

    		  try (ImageAnnotatorClient client = ImageAnnotatorClient.create()) {
    		    BatchAnnotateImagesResponse response = client.batchAnnotateImages(requests);
    		    List<AnnotateImageResponse> responses = response.getResponsesList();

    		    for (AnnotateImageResponse res : responses) {
    		      if (res.hasError()) {
    		        System.out.println(res.getError().getMessage());
    		        return;
    		      }

    		      // For full list of available annotations, see http://g.co/cloud/vision/docs
    		      for (EntityAnnotation annotation : res.getTextAnnotationsList()) {
    		    	  System.out.println(annotation.getDescription());
    		    	  //System.out.println(annotation.getBoundingPoly());
    		      }
    		    }
    		  }
    		}

		
    }

